package com.model;

public class Validations {
	
	private int id;
	private boolean status;
	private String userName;
	public Validations(int userId, boolean status, String userName) {
		this.id = userId;
		this.status = status;
		this.userName = userName;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
