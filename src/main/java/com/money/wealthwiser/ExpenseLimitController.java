package com.money.wealthwiser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ExpenseLimitDAO;
import com.model.ExpenseLimit;

@RestController
public class ExpenseLimitController {
	
	@Autowired
	ExpenseLimitDAO expenseLimitDAO;
	
	@GetMapping("/get-all-limits")
	public List<ExpenseLimit> getAllLimits(){
		return expenseLimitDAO.getAllLimits();
	}
}
