package com.money.wealthwiser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.BalanceDAO;
import com.model.Balance;

@CrossOrigin
@RestController
public class BalanceController {
	@Autowired
	BalanceDAO balanceDAO;
	
	@PostMapping("/set-balance")
	public Balance setBalance(@RequestBody Balance balance){
		return balanceDAO.setBalnace(balance);
	}
	
	@GetMapping("/get-balance-details/{userId}")
	public List<Balance> getBalance(@PathVariable int userId){
		return balanceDAO.getBalance(userId);
	}
	
	@GetMapping("/get-balance-by-month/{month}/{userId}")
	public Balance getBalance(@PathVariable String month, @PathVariable int userId){
		return balanceDAO.getBalanceByMonth(month, userId);
	}
	
	@DeleteMapping("/delete-balance/{id}")
	public void deleteBalnce(@PathVariable int id){
		balanceDAO.deleteBalance(id);
	}
}
