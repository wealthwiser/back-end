package com.money.wealthwiser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ExpenseDAO;
import com.model.Expense;

@CrossOrigin
@RestController
public class ExpenseContoller {
	
	@Autowired
	ExpenseDAO expenseDAO;
	
	@GetMapping("/get-expense-by-id/{id}")
	public Expense getAllExpenses(@PathVariable int id){
		return expenseDAO.getExpenseById(id);
	}
	
	@GetMapping("/get-expense-by-userid/{id}")
	public List<Expense> getExpensesByUserId(@PathVariable int id){
		return expenseDAO.getAllExpensesByUserId(id);
	}
	
	@GetMapping("/get-expense-by-month/{month}/{userId}")
	public List<Expense> getExpenseByMonth(@PathVariable int month, @PathVariable int userId){
		return expenseDAO.getAllExpensesByMonth(month, userId);
	}
	
	@PostMapping("/add-expense")
	public Expense addExpense(@RequestBody Expense expense){
		return expenseDAO.addExpense(expense);
	}
	
	@PutMapping("/update-expense")
	public Expense updateExpense(@RequestBody Expense expense){
		return expenseDAO.updateExpense(expense);
	}
	
	@DeleteMapping("/delete-expense/{id}")
	public String deleteExpense(@PathVariable int id){
		return expenseDAO.deleteExpense(id);
	}
}
