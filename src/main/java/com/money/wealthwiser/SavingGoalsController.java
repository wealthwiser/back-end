package com.money.wealthwiser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.SavingGoalsDAO;
import com.model.SavingGoals;

@CrossOrigin
@RestController
public class SavingGoalsController {
	@Autowired
	SavingGoalsDAO savingGoalsDAO;
	
	@GetMapping("/get-all-saving-goals/{userId}")
	public List<SavingGoals> getAllSavingGoals(@PathVariable int userId){
		return savingGoalsDAO.getAllSavingGoals(userId);
	}
	
	@PostMapping("/add-saving-goal")
	public SavingGoals addSavingGoal(@RequestBody SavingGoals goal){
		return savingGoalsDAO.addSavingGoal(goal);
	}

	@PutMapping("/update-saving-goal")
	public SavingGoals updateSavingGaol(@RequestBody SavingGoals goal){
		return savingGoalsDAO.updateSavingGoal(goal);
	}
	
	@DeleteMapping("/delete-saving-goal/{id}")
	public String deleteSavingGoal(@PathVariable int id){
		return savingGoalsDAO.deleteSavingGoal(id);
	}
}
