package com.money.wealthwiser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.User;
import com.model.Validations;

@CrossOrigin
@RestController
public class UserController {
	
	@Autowired
	UserDAO userDAO;
	
	@PostMapping("/register-user")
	public User registerUser(@RequestBody User user){
		return userDAO.registerUser(user);
	}
	
	@GetMapping("/get-users")
	public List<User> getUsers(){
		return userDAO.getUsers();
	}
	
	@GetMapping("/user-login/{email}/{password}")
	public Validations validateUser(@PathVariable String email,@PathVariable String password){
		return userDAO.validateUser(email, password);
	}
	
	@PutMapping("/update-password/{id}/{password}/{newPassword}")
	public boolean updatePassword(@PathVariable int id,@PathVariable String password, @PathVariable String newPassword){
		return userDAO.updatePassword(id, password, newPassword);
	}
	
	
	
}
