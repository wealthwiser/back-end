package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.User;
import com.model.Validations;

@Service
public class UserDAO {
	@Autowired
	UserRepo userRepo;
	PasswordEncoder passwordEncoder;
	
	public UserDAO(){
		this.passwordEncoder = new BCryptPasswordEncoder();
	}
	
	public User registerUser(User user){
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return userRepo.save(user);
	}
	
	public List<User> getUsers(){
		return userRepo.findAll();
	}
	
	public boolean updatePassword(int id, String password, String newPassword){
		User user = userRepo.findById(id).orElse(null);
		if(passwordEncoder.matches(password, user.getPassword())){
			user.setPassword(passwordEncoder.encode(newPassword));
			userRepo.save(user);
			return true;
		}
		return false;
	}
	
	
	public Validations validateUser(String email, String password){
		User user = userRepo.findByEmail(email);
		if(user == null){
			return new Validations(0, false, "");
		}
		return new Validations(user.getUserId(), passwordEncoder.matches(password, user.getPassword()), user.getUserName());
	}
	
}
