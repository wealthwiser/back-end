package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.SavingGoals;

@Service
public class SavingGoalsDAO {
	
	@Autowired
	SavingGoalsRepo savingGoalsRepo; 
	
	public List<SavingGoals> getAllSavingGoals(int userId){
		return savingGoalsRepo.findAllGoalsById(userId);
	}
	
	public SavingGoals addSavingGoal(SavingGoals goal){
		return savingGoalsRepo.save(goal);
	}
	
	public SavingGoals updateSavingGoal(SavingGoals goal){
		if(savingGoalsRepo.findById(goal.getId()).orElse(null) != null){
			return savingGoalsRepo.save(goal);
		}
		return null;
	}
	
	public String deleteSavingGoal(int id){
		savingGoalsRepo.deleteById(id);
		return "Deleted successfull!";
	}
}
