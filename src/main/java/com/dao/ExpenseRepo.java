package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Expense;

@Repository
public interface ExpenseRepo extends JpaRepository<Expense, Integer> {
	
	@Query(value = "SELECT * FROM expense WHERE user_id = :userId", nativeQuery = true)
	List<Expense> findAllExpnsesById(@Param("userId") int userId);
	
	@Query(value = "SELECT * FROM expense WHERE month(date)= :month and user_id = :userId", nativeQuery = true)
	List<Expense> findAllExpnsesByMonth(@Param("month") int month, @Param("userId") int userId);
}
