package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.SavingGoals;

@Repository
public interface SavingGoalsRepo extends JpaRepository<SavingGoals, Integer> {
	@Query(value = "SELECT * FROM saving_goals WHERE user_id = :userId", nativeQuery = true)
	List<SavingGoals> findAllGoalsById(@Param("userId") int userId);
}
