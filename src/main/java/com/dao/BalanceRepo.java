package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Balance;

@Repository
public interface BalanceRepo extends JpaRepository<Balance, Integer> {
	
	@Query(value="SELECT * FROM balance WHERE user_id = :userId and month = :month", nativeQuery=true)
	Balance findByMonth(@Param("month") String month, @Param("userId") int userId);

	@Query(value="SELECT * FROM balance WHERE user_id = :userId", nativeQuery=true)
	List<Balance> findAllByUserId(@Param("userId") int userId);
}
