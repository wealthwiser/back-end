package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Expense;

@Service
public class ExpenseDAO {
	@Autowired
	ExpenseRepo expenseRepo;
	
	public Expense getExpenseById(int id){
		return expenseRepo.findById(id).orElse(null);
	}
	
	public List<Expense> getAllExpensesByUserId(int userId){
		return expenseRepo.findAllExpnsesById(userId);
	}
	
	public List<Expense> getAllExpensesByMonth(int month, int userId){
		return expenseRepo.findAllExpnsesByMonth(month, userId);
	}
	
	public Expense addExpense(Expense expense){
		return expenseRepo.save(expense);
	}
	
	public Expense updateExpense(Expense expense){
		if(expenseRepo.findById(expense.getId()).orElse(null) != null){
			return expenseRepo.save(expense);
		}
		return null;
	}
	
	public String deleteExpense(int id){
		expenseRepo.deleteById(id);
		return "Deleted successfully";
	}
}
