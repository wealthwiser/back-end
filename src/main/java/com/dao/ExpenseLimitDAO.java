package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.ExpenseLimit;

@Service
public class ExpenseLimitDAO {
	@Autowired
	ExpenseLimitsRepo expenseLimitsRepo;
	
	public List<ExpenseLimit> getAllLimits(){
		return expenseLimitsRepo.findAll();
	}
	
	public ExpenseLimit addLimit(ExpenseLimit expenseLimit){
		return expenseLimitsRepo.save(expenseLimit);
	}
	
	public ExpenseLimit updateLimit(ExpenseLimit expenseLimit){
		int id = expenseLimit.getId();
		if(expenseLimitsRepo.findById(id) != null) return expenseLimitsRepo.save(expenseLimit);
		return new ExpenseLimit();
	}
	
	public String deleteLimit(int id){
		expenseLimitsRepo.deleteById(id);
		return "deleted successfully!";
	}
}
