package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Balance;

@Service
public class BalanceDAO {
	
	@Autowired
	BalanceRepo balanceRepo;
	
	public Balance setBalnace(Balance balance){
		return balanceRepo.save(balance);
	}
	
	public List<Balance> getBalance(int userId){
		return balanceRepo.findAllByUserId(userId);
	}
	
	public Balance getBalanceByMonth(String month, int userId){
		return balanceRepo.findByMonth(month, userId);
	}
	
	public void deleteBalance(int id){
		balanceRepo.deleteById(id);
	}
}
